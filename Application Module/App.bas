﻿Type=StaticCode
Version=4.7
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
'Static code module
Sub Process_Globals
	Public ApplicationName, ApplicationDescription, ApplicationVersion, ApplicationURL As String
	Public CopyrightName, CopyrightYear As String
	Private AppInfoLV As ListView
	#If B4J
	Private FX As JFX
	#End If
End Sub

#If B4J
Public Sub ShowAbout (CenterForm As Boolean, CenterInfo As Boolean)
	Dim AF As Form
	AF.Initialize("About", 500, 500)
	AppInfoLV.Initialize("AppInfoLV")
	If ApplicationURL.Contains("http://") = False Then ApplicationURL = "http://" & ApplicationURL
	Dim Copyright As String = $"Copyright (C) ${CopyrightName}, ${CopyrightYear}"$
	AppInfoLV.Items.AddAll(Array As String(ApplicationName, Copyright, ApplicationDescription, $"Website: ${ApplicationURL}"$))	
	AF.RootPane.AddNode(AppInfoLV, 0, 0,AF.Width, AF.Height)
	If CenterForm Then FormTools.CenterForm(AF)
	If CenterInfo Then FormTools.ListViewAlignCenter(AF, AppInfoLV)
	AF.Show
End Sub

Private Sub AppInfoLV_MouseClicked (EventData As MouseEvent)
	If EventData.ClickCount = 2 Then
		Dim SI As String = AppInfoLV.SelectedItem
		If SI.Contains("Website: ") Then
			FX.ShowExternalDocument(ApplicationURL.Replace("Website: ", ""))
		End If
	End If
End Sub
#End If