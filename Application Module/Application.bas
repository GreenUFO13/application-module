﻿Type=Class
Version=4.7
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
Sub Class_Globals
	Public ApplicationName, ApplicationDescription, ApplicationVersion, ApplicationURL As String
	Public CopyrightName, CopyrightYear As String
	#If B4J
	Private fx As JFX
	#End If
	Private MainForm As Form
	Private NameLabel, CopyrightLabel, WebsiteLabel As Label
	Private DescriptionTA As TextArea
	Private OKButton As Button
End Sub

'Returns the form so you can apply your theme, center the form, etc.
Public Sub AboutForm As Form
	Return MainForm
End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize
	MainForm.Initialize("About", 500, 500)
	MainForm.Title = $"About ${ApplicationName}"$
	MainForm.RootPane.LoadLayout("AboutForm")
End Sub

Public Sub ShowAbout
	NameLabel.Text = ApplicationName
	If ApplicationURL.Contains("http://") = False Then
		ApplicationURL = "http://" & ApplicationURL
	End If
	WebsiteLabel.Text = ApplicationURL
	CopyrightLabel.Text = $"Copyright (C) ${CopyrightName}, ${CopyrightYear}"$
	DescriptionTA.Text = ApplicationDescription
	MainForm.Show
End Sub

Private Sub WebsiteLabel_MouseClicked (EventData As MouseEvent)
	If EventData.ClickCount = 2 Then
		Log ("DC")
		fx.ShowExternalDocument(WebsiteLabel.Text)
	End If
End Sub

Private Sub OKButton_Action
	MainForm.Close
End Sub